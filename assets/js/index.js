var ws = null
var useSecureConnection = true

const Delay = (ms = 0) => new Promise((resolve, reject) => setTimeout(() => resolve(), ms))

function cleanSlate() {
  $("#lastReceivedMessageTimestamp").text("")
  $("#lastReceivedMessage").text("")
  $("#lastSendMessageTimestamp").text("")
  $("#lastSendMessage").text("")
  $("#sendMessageSpan").css("color", "unset")
}

function websocketReady() {
  return (ws !== null && ws.readyState === WebSocket.OPEN)
}

function setWSInputs(enable = true) {
  if (enable) {
    $("#useTLS").removeAttr("disabled")
    $("#host").removeAttr("disabled")
    $("#port").removeAttr("disabled")
  } else {
    $("#useTLS").attr("disabled", "disabled")
    $("#host").attr("disabled", "disabled")
    $("#port").attr("disabled", "disabled")
  }
}

function updateStatus(msg, color = "unset") {
  $("#wsStatus").text(msg)
  $("#wsStatus").removeClass("text-secondary")
  $("#wsStatus").css("color", "unset")
  if (color !== "unset") $("#wsStatus").css("color", color)
  else $("#wsStatus").addClass("text-secondary")
}

function addWSMessage(msg, incoming = true, outgoingSuccess = true) {
  let now = Date.now()
  if (incoming) {
    $("#lastReceivedMessage").text(msg)
    $("#lastReceivedMessageTimestamp").text(now)
    $("#messageHistory").prepend(`<span>[${now}][Incoming] <span class="text-secondary">${(typeof msg === "object") ? JSON.stringify(msg) : msg}</span></span>`)
  } else {
    $("#lastSendMessage").text(msg)
    $("#lastSendMessageTimestamp").text(now)
    $("#messageHistory").prepend(`<span style="color:${(outgoingSuccess) ? "var(--bs-success)" : "var(--bs-danger)"};">[${now}][Outgoing] ${(typeof msg === "object") ? JSON.stringify(msg) : msg}</span>`)
    $("#sendMessageSpan").css("color", (outgoingSuccess) ? "var(--bs-success)" : "var(--bs-danger)")
  }
}

function sendWebsocketMessage(message) {
  try {
    if (message === "") return
    if (!websocketReady()) return toastr.error("Websocket connection is not ready yet.", "Can't send websocket message")
    ws.send(message)
    $("#message").val("")
    addWSMessage(message, false, true)
  } catch (e) {
    console.error("Failed to send websocket message.")
    console.error(message)
    console.error(e)
    addWSMessage(message, false, false)
  }
}

async function connect_websocket(htmlEvent) {
  if (websocketReady()) {
    disconnectWebsocket()
    await Delay(100)
  }
  setWSInputs(false)
  updateStatus("Preparing to connect...")
  let host = $("#host").val()
  let port = $("#port").val()
  if (!port || port === "") port = 80
  let wsURL = `${(useSecureConnection) ? "wss" : "ws"}://${host}:${port}`
  $("#wsURL").text(wsURL)
  $("#usingTLS").text((useSecureConnection) ? "Yes" : "No")
  updateStatus("Connecting...")
  let startTimer = Date.now()
  ws = new WebSocket(wsURL)
  ws.onopen = async (event) => {
    updateStatus(`Websocket connected in ${Date.now()-startTimer}ms`, "var(--bs-success)")
  }
  ws.onmessage = async (event) => {
    if (event.data.startsWith("heartbeat")) return ws.send(event.data)
    let message = event.data
    try {
      message = JSON.parse(message)
    } catch (e) {
      // Oh.. nothing.
    }
    console.debug("Server send me a WS message <3")
    console.debug(message)
    addWSMessage(message)
  }
  ws.onclose = (event) => {
    console.debug({"onCloseEvent": event})
    let message = "Connection closed"
    if (event.wasClean) updateStatus(message)
    else {
      message = `Connection failed in ${Date.now()-startTimer}ms`
      updateStatus(message, "var(--bs-danger)")
    }
    addWSMessage(message)
    $("#messageHistory").prepend(`<span style="color:var(--bs-gray);">--- History of connection ${$("#wsURL").text()} ---</span>`)
    setWSInputs(true)
  }
}

function disconnectWebsocket() {
  updateStatus("Closing connection")
  ws.close()
}

function resizeWindow() {
  // let box = document.querySelector('.box');
  $("#messageHistory").css("height", "0px")
  let nPix = ($("#elements").height() + $("#title").height()) - $("#messageHistoryContainer").height()
  let calc = ($(window).height() - nPix - 120)
  console.debug({"elements": $("#elements").height(), "title": $("#title").height(), "window": $(window).height(), "calc": calc, "messageHistoryContainer": $("#messageHistoryContainer").height()})
  // Windows height - Current elements height - offset
  if (calc > 200) $("#messageHistory").css("height", `calc(${$(window).height()}px - ${nPix}px - 120px)`)
  else $("#messageHistory").css("height", `200px`)
}

$(() => {
  $("#useTLS").on("change", (event) => useSecureConnection = $("#useTLS").is(":checked"))
  $("#message").on("keydown", (event) => {
    if (event.key === "Enter") event.preventDefault()
  })
  $("#message").on("keyup", (event) => {
    if (event.key !== "Enter") return true
    event.preventDefault()
    sendWebsocketMessage($("#message").val())
  })
  resizeWindow()
  $(window).on("resize", resizeWindow)
})