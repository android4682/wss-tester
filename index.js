// Global variables
var isDebugMode = false
var isDevMode = false

// Custom log handling
const terminalColors = {
  brighter: "\x1b[1m",
  darker: "\x1b[2m",
  reset: "\x1b[0m",
  white: "\x1b[37m",
  lightWhite: "\x1b[0m\x1b[1m\x1b[37m",
  darkWhite: "\x1b[0m\x1b[2m\x1b[37m",
  red: "\x1b[31m",
  lightRed: "\x1b[0m\x1b[1m\x1b[31m",
  darkRed: "\x1b[0m\x1b[2m\x1b[31m",
  yellow: "\x1b[33m",
  lightYellow: "\x1b[0m\x1b[1m\x1b[33m",
  darkYellow: "\x1b[0m\x1b[2m\x1b[33m",
  cyan: "\x1b[36m",
  lightCyan: "\x1b[0m\x1b[1m\x1b[36m",
  darkCyan: "\x1b[0m\x1b[2m\x1b[36m",
  blue: "\x1b[34m",
  lightBlue: "\x1b[0m\x1b[1m\x1b[34m",
  darkBlue: "\x1b[0m\x1b[2m\x1b[34m",
  green: "\x1b[32m",
  lightGreen: "\x1b[0m\x1b[1m\x1b[32m",
  darkGreen: "\x1b[0m\x1b[2m\x1b[32m",
  purple: "\x1b[35m",
  lightPurple: "\x1b[0m\x1b[1m\x1b[35m",
  darkPurple: "\x1b[0m\x1b[2m\x1b[35m",
  gray: "\x1b[2m\x1b[37m",
  lightGray: "\x1b[0m\x1b[1m\x1b[37m",
  darkGray: "\x1b[0m\x1b[2m\x1b[37m"
}
const orgConsole = {
  "warn": console.warn,
  "error": console.error,
  "debug": console.debug,
  "log": console.log,
  "dir": console.dir
}
console.dir = (input, log = false) => {
  orgConsole.dir(input)
  return input
}
console.warn = (input, log = true) => {
  if (typeof input === "object") console.dir(input, log)
  else orgConsole.warn(`${terminalColors.yellow}${input || ""}${terminalColors.reset}`)
  return input
}
console.error = (input, log = true) => {
  if (typeof input === "object") console.dir(input, log)
  else orgConsole.error(`${terminalColors.red}${input || ""}${terminalColors.reset}`)
  return input
}
console.debug = (input, log = false) => {
  if (!isDebugMode) return input
  if (typeof input === "object") console.dir(input, log)
  else orgConsole.debug(`${terminalColors.purple}${input || ""}${terminalColors.reset}`)
  return input
}
console.success = (input, log = true) => {
  if (typeof input === "object") console.dir(input, log)
  else orgConsole.log(`${terminalColors.green}${input || ""}${terminalColors.reset}`)
  return input
}
console.info = (input, log = true) => {
  if (typeof input === "object") console.dir(input, log)
  else orgConsole.log(`${terminalColors.lightBlue}${input || ""}${terminalColors.reset}`)
  return input
}
console.log = (input, log = true) => {
  if (typeof input === "object") console.dir(input, log)
  else orgConsole.log(`${input || ""}`)
  return input
}

// Flags - tmp.flags
if (process.argv.indexOf("--debug") !== -1) {
  isDebugMode = true
  console.debug("Debug mode enabled!")
}
if (process.argv.indexOf("--development") !== -1 || process.argv.indexOf("--dev") !== -1) {
  isDevMode = true
  console.success("Dev mode enabled!")
}

// Loading modules - tmp.modules
// Load express module
const express = require("express")
console.info("Module 'express' loaded as 'express'.")
// Load fs module
const fs = require("fs")
console.info("Module 'fs' loaded as 'fs'.")
// Load https module
const http = require("http")
console.info("Module 'http' loaded as 'http'.")
// Load https module
const https = require("https")
console.info("Module 'https' loaded as 'https'.")
// Load path module
const path = require("path")
console.info("Module 'path' loaded as 'path'.")
// Load ws module
// const WebSocketClient = require('websocket').client;
// console.info("Module 'websocket' loaded only class 'client' as 'WebSocketClient'.")

function errorLog(e) {
  let msg = e
  let stacktrace
  if (typeof e === "object") {
    if (!!e.message) {
      msg = e.message
      stacktrace = e.stack
    } else if (!!e.sqlMessage) {
      msg = e.sqlMessage
      stacktrace = e
    } else stacktrace = e
  }
  console.error("Error: " + msg)
  console.error(stacktrace)
}

// Prototypes of loaded modules - tmp.proto.modules
/**
 * @abstract
 * @param {!String} path The path to a file you want to read
 * @param {?Object} [options={}] An object with `fs.readFile()` options
 * @returns {!Promise} Returns a Promise. On resolve it returns the read data, reject returns a error if any
 * @see fs.readFile()
 * @see fs.readFileSync()
 * @requires fs
 * @desc Same as `fs.readFileSync()` but with error handling support
 */
const readFileSync = (path, options) => new Promise((resolve, reject) => fs.readFile(path, options, (err, dataBuffer) => {
  if (err) reject(err)
  resolve(dataBuffer.toString("utf8"))
}))

function findObject(arr, keys, values) {
  let found = false
  if (Array.isArray(arr)) {
    for (let i = 0; i < arr.length; i++) {
      const obj = arr[i];
      for (let j = 0; j < keys.length; j++) {
        const key = keys[j];
        if (obj[key] === values[j]) {
          found = true
          break
        }
      }
      if (found === true) return {...obj, __index: i}
    }
  } else {
    for (const key in arr) {
      if (Object.hasOwnProperty.call(arr, key)) {
        const child = arr[key];
        for (let i = 0; i < keys.length; i++) {
          const key = keys[i];
          if (child[key] === values[i]) {
            found = true
            break
          }
        }
        if (found === true) return {...child, __key: key}
      }
    }
  }
  return null
}

// Classes
class NoBSArray extends Array {
  constructor(...args) {
    super()
    this.import(args)
  }
  
  import(args) {
    if (args.length > 0) for (let i = 0; i < args.length; i++) this.push(args[i])
    return this
  }

  has(match) {
    return (this.indexOf(match) !== -1)
  }

  /**
   * Find a string in the array matching given regex
   * @param {String} match String of regex to match with string
   * @return {String|null} Returns matching string or null if nothing is found
   */
  findReg(match) {
    let reg = new RegExp(match)
    return this.filter((item) => {
      return (typeof item == 'string' && item.match(reg))
    })
  }

  /**
   * Removed value from array
   * @param {*} value Value to look for and remove out of array
   * @return {Boolean} Returns true on success (removed value or value not found) and false if the given value is empty
   * @description Removed value from array. Note that this function doesn't stop at the first match!
   */
  removeByValue(value) {
    if (isEmpty(value)) return false
    for (let i = this.length-1; i >= 0; i--) {
      const element = this[i];
      if (element === value) this.splice(i, 1)
    }
    return true
  }
}

class Request {
  static async request(options) {
    return new Promise((resolve, reject) => {
      if (typeof options.data === "object") options.data = JSON.stringify(options.data)
      else if (typeof options.data !== "string" && !isEmpty(options.data)) options.data = options.data.toString()
      let httpHandler = (options.url.startsWith("http://")) ? http : https
      let request = httpHandler.request(options, (res) => {
        let chunks = []
        res.on('data', (chunk) => chunks.push(chunk))
        res.on('end', () => {
          let body = Buffer.concat(chunks).toString()
          if (res.headers['content-type'].includes("application/json")) body = JSON.parse(body);
          if (res.statusCode != 200) reject({
            error: res.statusMessage,
            "status": res.statusCode,
            res: body
          })
          else resolve({
            body,
            "headers": res.headers,
            "status": res.statusCode
          })
        })
      })
      request.on("error", (e) => reject({
        error: e,
        "status": 500,
        res: null
      }))
      if (!isEmpty(options.data)) request.write(options.data)
      request.end()
    })
  }

  static async get(uri, headers = {}) {
    return new Promise((resolve, reject) => {
      let httpHandler = (uri.startsWith("http://")) ? http : https
      let request = httpHandler.request(uri, {
        "method": "GET",
        headers
      }, (res) => {
        let chunks = []
        res.on('data', (chunk) => chunks.push(chunk))
        res.on('end', () => {
          let body = Buffer.concat(chunks).toString()
          if (res.headers['content-type'].includes("application/json")) body = JSON.parse(body);
          if (res.statusCode != 200) reject({
            error: res.statusCode,
            res: body
          })
          else resolve(body)
        })
      })
      request.on("error", (e) => reject({
        error: e,
        res: {
          statusCode: 500
        }
      }))
      request.end()
    })
  }

  static async post(uri, headers = {}, body, contentType = "text/plain") {
    let me = this
    return new Promise((resolve, reject) => {
      if (typeof body === "object") {
        if (contentType === "application/x-www-form-urlencoded") body = me.toUrlEncoded(body)
        else {
          body = JSON.stringify(body)
          contentType = "application/json"
        }
      } else if (typeof body !== "string" && !isEmpty(body)) body = body.toString()
      else body = ""
      let httpHandler = (uri.startsWith("http://")) ? http : https
      let request = httpHandler.request(uri, {
        "method": "POST",
        "headers": {
          "Content-Type": contentType,
          ...headers
        }
      }, (res) => {
        let chunks = []
        res.on('data', (chunk) => chunks.push(chunk))
        res.on('end', () => {
          let body = Buffer.concat(chunks).toString()
          if (res.headers['content-type'].includes("application/json")) body = JSON.parse(body);
          if (res.statusCode != 200) reject({
            error: res.statusCode,
            res: body
          })
          else resolve({
            body,
            "headers": res.headers
          })
        })
      })
      request.on("error", (e) => reject({
        error: e,
        res: {
          statusCode: 500
        }
      }))
      request.write(body)
      request.end()
    })
  }

  static createArgumentString(args = {}) {
    let argStr = ""
    let amount = 0
    for (const key in args) {
      if (Object.hasOwnProperty.call(args, key)) {
        const arg = args[key];
        if (isEmpty(arg)) continue
        if (amount === 0) argStr += `?`
        else argStr += `&`
        argStr += `${key}=${arg}`
        amount++
      }
    }
    return argStr
  }

  static toUrlEncoded(obj) {
    return Object.keys(obj).map(k => encodeURIComponent(k) + '=' + encodeURIComponent(obj[k])).join('&')
  }
}

// Loading config variables
var expressConfig
if (!fs.existsSync("./config.js")) {
  console.error("Config file couldn't be found. Loading default from hard-code...")
  expressConfig = {
    "port": 8443,
    "host": "127.0.0.1",
    "keyFile": "server.key",
    "certFile": "server.cert",
    "certificateIsSelfSigned": false
  }
  console.error("WARNING! Loading from hard-code might not be as optimal as a standalone config file.\nPlease create a config file!!")
} else {
  expressConfig = require("./config.js")
  console.success("Config loaded from file!")
}

// Config location/files check

// General functions - tmp.gfunc
const Sleep = (ms) => new Promise((resolve, reject) => setTimeout(() => resolve(), ms))

/**
 * My famous (not really) isEmpty function. It checks if a given variable is empty or not
 * @param {*} variable Anything you want to know if it's empty or not
 * @returns {Boolean} Returns true on empty and false on not empty
 */
function isEmpty(variable) {
  if (variable == null || variable == undefined || Number.isNaN(variable)) return true
  else if (typeof variable == "string" && variable == "") return true
  else if (typeof variable == "object") {
    if (Array.isArray(variable) && variable.length == 0) return true
    else if (Object.keys(variable).length == 0) return true
  }
  return false
}

// Program's helper functions
function htmlRenderer(res, localPath, render = {}) {
  res.statusCode = 200
  res.setHeader('Content-Type', 'text/html')
  res.render(path.join(localPath), render)
}

function error_page_handling(res, statusCode = 500) {
  res.statusCode = statusCode
  res.end("Error: " + statusCode)
}

function resAPIHandling(res, {success = null, data = null, error = null}, statusCode = 200) {
  if (success === undefined) success = null
  if (data === undefined) data = null
  if (error === undefined) error = null
  res.statusCode = statusCode
  res.setHeader('Content-Type', 'application/json')
  res.json({success, data, error})
}

// Program specific variables
const app = express()

app.use(express.urlencoded({extended: true}))
app.use(express.json({extended: true}));

app.use("/css/", express.static(__dirname + '/assets/css'));
// app.use("/css/", (req, res) => dynamicAssetLoading(req.url, res, "/assets/css"));
app.use("/fontawesome/", express.static(__dirname + '/assets/webfonts'));
app.use("/webfonts/", express.static(__dirname + '/assets/webfonts'));
// app.use("/js/", (req, res) => dynamicAssetLoading(req.url, res, "/assets/js"));
app.use("/js/", express.static(__dirname + '/assets/js'));
app.use("/images/", express.static(__dirname + '/assets/images'));
app.use("/img/", express.static(__dirname + '/assets/images'));
app.use("/fonts/", express.static(__dirname + '/assets/fonts'));
app.use("/favicon/", express.static(__dirname + '/assets/favicon'));

// app.set('view engine', 'ejs');

// Program's (handler) functions
app.get("/", (req, res) => res.sendFile(path.join(__dirname, "assets/html/index.html")))

// Code execution
var server
async function bootUp() {
  try {
    // HTTP Server
    let httpsCredentials = {}
    let httpClass = http
    let useHttps = false
    if (fs.existsSync(expressConfig.keyFile) && fs.existsSync(expressConfig.certFile)) {
      httpsCredentials = {
        key: await readFileSync(expressConfig.keyFile),
        cert: await readFileSync(expressConfig.certFile)
      }
      httpClass = https
      useHttps = true
    }
    else console.warn("Missing certificate and/or key file to create a HTTPS server. Running without SSL. (You can easily create self-signed certificate by running `npm run gen-ssl`)")
    // If this server has a self-signed certificated ignore SSC's
    if (useHttps) {
      process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0
      console.debug(`Setting 'process.env["NODE_TLS_REJECT_UNAUTHORIZED"]' to '0' because server certificate is set to 'self-signed'`)
    }
    // Once all is done catch any other endpoints that aren't catched
    app.get("*", (req, res) => error_page_handling(res, 404))
    app.post("*", (req, res) => res.json({"error": 404, "egg": "GETTEGG"}))
    server = httpClass.createServer(httpsCredentials, app).listen(expressConfig.port, expressConfig.host)
    console.success(`${(useHttps) ? "HTTPS" : "HTTP"} server running on ${expressConfig.host}:${expressConfig.port}. Waiting for requests...`)
  } catch (e) {
    errorLog(e)
    process.exit(1)
  }
}
bootUp()